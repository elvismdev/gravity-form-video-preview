<?php

class GCTVVideoPreviewOptions {
	private $gctv_video_preview_options;

	public function __construct() {
		add_action( 'admin_menu', array( $this, 'gctv_video_preview_add_plugin_page' ) );
		add_action( 'admin_init', array( $this, 'gctv_video_preview_page_init' ) );
	}

	public function gctv_video_preview_add_plugin_page() {
		add_options_page(
			'GCTV Video Preview', // page_title
			'GCTV Video Preview', // menu_title
			'manage_options', // capability
			'gctv-video-preview', // menu_slug
			array( $this, 'gctv_video_preview_create_admin_page' ) // function
		);
	}

	public function gctv_video_preview_create_admin_page() {
		$this->gctv_video_preview_options = get_option( 'gctv_video_preview_option_name' ); ?>

		<div class="wrap">
			<h2>GCTV Video Preview</h2>
			<p></p>
			<?php settings_errors(); ?>

			<form method="post" action="options.php">
				<?php
					settings_fields( 'gctv_video_preview_option_group' );
					do_settings_sections( 'gctv-video-preview-admin' );
					submit_button();
				?>
			</form>
		</div>
	<?php }

	public function gctv_video_preview_page_init() {
		register_setting(
			'gctv_video_preview_option_group', // option_group
			'gctv_video_preview_option_name', // option_name
			array( $this, 'gctv_video_preview_sanitize' ) // sanitize_callback
		);

		add_settings_section(
			'gctv_video_preview_setting_section', // id
			'Settings', // title
			array( $this, 'gctv_video_preview_section_info' ), // callback
			'gctv-video-preview-admin' // page
		);

		add_settings_field(
			'preview_time_0', // id
			'Preview Time (in seconds)', // title
			array( $this, 'preview_time_0_callback' ), // callback
			'gctv-video-preview-admin', // page
			'gctv_video_preview_setting_section' // section
		);

		add_settings_field(
			'gravity_form_id_1', // id
			'Gravity Form ID', // title
			array( $this, 'gravity_form_id_1_callback' ), // callback
			'gctv-video-preview-admin', // page
			'gctv_video_preview_setting_section' // section
		);
	}

	public function gctv_video_preview_sanitize($input) {
		$sanitary_values = array();
		if ( isset( $input['preview_time_0'] ) ) {
			$sanitary_values['preview_time_0'] = sanitize_text_field( $input['preview_time_0'] );
		}

		if ( isset( $input['gravity_form_id_1'] ) ) {
			$sanitary_values['gravity_form_id_1'] = sanitize_text_field( $input['gravity_form_id_1'] );
		}

		return $sanitary_values;
	}

	public function gctv_video_preview_section_info() {
		
	}

	public function preview_time_0_callback() {
		printf(
			'<input class="regular-text" type="text" name="gctv_video_preview_option_name[preview_time_0]" id="preview_time_0" value="%s">',
			isset( $this->gctv_video_preview_options['preview_time_0'] ) ? esc_attr( $this->gctv_video_preview_options['preview_time_0']) : ''
		);
	}

	public function gravity_form_id_1_callback() {
		printf(
			'<input class="regular-text" type="text" name="gctv_video_preview_option_name[gravity_form_id_1]" id="gravity_form_id_1" value="%s">',
			isset( $this->gctv_video_preview_options['gravity_form_id_1'] ) ? esc_attr( $this->gctv_video_preview_options['gravity_form_id_1']) : ''
		);
	}

}