<?php
/**
 * Plugin Name: GCTV Video Preview
 * Plugin URI: https://bitbucket.org/grantcardone/gctv-video-preview
 * Description: Enables the preview of premium content show videos for a short period of time, then it stops the video playback and shows an overlay asking for subscription to watch the full show. Uses some of the JWPlayer API.
 * Version: 1.0
 * Author: Elvis Morales
 * Author URI: https://twitter.com/n3rdh4ck3r
 * Requires at least: 3.5
 * Tested up to: 4.3
 */
if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

define( 'GCTVVP_PLUGIN_PATH', plugin_dir_path( __FILE__ ) );

require_once( GCTVVP_PLUGIN_PATH . 'inc/plugin-options.php' );

if ( is_admin() )
	$gctvvp_options_page = new GCTVVideoPreviewOptions();


function gctvvp_set_video_preview() {
	global $post;

	$terms = get_the_terms($post->ID, 'category');
	foreach( $terms as $key => $term ) {
		if( $term->slug == 'featured' )
			unset( $terms[$key] );
	}

	if( !empty( $terms ) ) {
		$term = array_pop($terms);
		$blocked = get_field('blocked', $term );

		if( $blocked == 'on' && $term->parent == 54 && !is_user_logged_in() ) {
			$gctv_video_preview_options = get_option( 'gctv_video_preview_option_name' );
			add_filter( 'gform_get_form_filter_'.$gctv_video_preview_options['gravity_form_id_1'], 'clean_gravity_form', 10, 2 ); ?>
			<script type="text/javascript">
				jwplayer().onPlay(function() {
					if (this.getPosition() >= <?= $gctv_video_preview_options['preview_time_0']; ?>) {
						this.seek(0);
					}
				});

				jwplayer().onSeek(function() {
					if (this.getPosition() >= <?= $gctv_video_preview_options['preview_time_0']; ?>) {
						this.play(false);
					}
				});

				jwplayer().onTime(function() {
					if (this.getPosition() >= <?= $gctv_video_preview_options['preview_time_0']; ?>) {
						this.play(false);
						jQuery('#player-embed').prepend('<div id="overlayblock1">\
							<div class="row text-center">\
								<img src="/wp-content/uploads/GCTV_2015.06.22_GCTV_Logo.png">\
							</div>\
							<div class="row text-center">\
								<span class="exclusive-text">Exclusive</span>\
								<span class="premium-text">Premium Content</span>\
							</div>\
							<div class="row text-center">\
								<div class="col-md-6 subscribe-wording">\
									<p class="wording-for-mobile">To continue watching... <a class="login-modal" href="#" data-toggle="modal" data-target="#myModal2">Tap here to Log In or Sign Up for your FREE subscription now.</a></p>\
									<p>To continue watching... sign up for your FREE subscription now.</p>\
									<p>A Grant Cardone TV subscription gives you full access to all of our free and premium shows including the most current episodes and full seasons of Whatever It Takes: The Ultimate Job Interview. Watch on all of your devices with your free premium subscription.</p>\
									<p>Already a subscriber? <a class="btn login-modal" href="#" data-toggle="modal" data-target="#myModal2">Log In</a></p>\
								</div>\
								<div class="col-md-6 the-subscribe-form">\
									<?php gravity_form( $gctv_video_preview_options['gravity_form_id_1'], false, false ); ?>\
								</div>\
							</div>\
						</div>');
}
});
</script>
<?php }
}
}

function clean_gravity_form( $form_string, $form ) {
	return str_replace( 
		array( PHP_EOL, "'" ), 
		array( "\\\n", "\'" ), 
		$form_string 
		);
}

add_action( 'wp_footer', 'gctvvp_set_video_preview' );